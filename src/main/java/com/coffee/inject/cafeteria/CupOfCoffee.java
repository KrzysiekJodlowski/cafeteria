package com.coffee.inject.cafeteria;

import com.coffee.inject.beans.Bean;

@Bean(implementation = Latte.class)
public interface CupOfCoffee {
  void drink();
}
