package com.coffee.inject.cafeteria.figures;

import com.coffee.inject.beans.Bean;

@Bean(implementation = Square.class)
public abstract class Figure {
  abstract void print();
}
