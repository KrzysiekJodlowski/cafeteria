package com.coffee.inject.cafeteria;

import com.coffee.inject.beans.Coffee;

public class Main {
  public static void main(String[] args) {
    Coffee coffee = Coffee.fromScannotation();
    CupOfCoffee cupOfCoffee = coffee.brew(CupOfCoffee.class);
    cupOfCoffee.drink();
  }
}
