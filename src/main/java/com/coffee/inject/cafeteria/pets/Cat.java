package com.coffee.inject.cafeteria.pets;

public class Cat implements Pet {
  @Override
  public void voice() {
    System.out.println("Meow!");
  }
}
