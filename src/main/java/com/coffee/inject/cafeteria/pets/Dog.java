package com.coffee.inject.cafeteria.pets;

public class Dog implements Pet {
  @Override
  public void voice() {
    System.out.println("Howl!");
  }
}
