package com.coffee.inject.cafeteria.pets;

import com.coffee.inject.beans.Bean;

@Bean(implementation = Dog.class)
public interface Pet {
  void voice();
}
